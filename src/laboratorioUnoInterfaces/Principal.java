/*
 * Archivo: Principal.java
 * Fecha creacion: 11 de febrereo de 2020
 * Fecha última modificacion: 11 de febrero de 2020
 * Autores: Jaimen Aza     
 *              Juan Camilo Quintero          
 */

package laboratorioUnoInterfaces;

import java.awt.EventQueue;
import javax.swing.UIManager;

/**
 * The Class Principal.
 * Clase principal para ejectuar el programa
 * Relación: tiene relación con la clase GUILaboratorio
 */
public class Principal {
	
	/**
	 * The main method.
	 * Método principal que ejecuta todo el programa.
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		try {
            String className = UIManager.getCrossPlatformLookAndFeelClassName();
            UIManager.setLookAndFeel(className);
        } catch (Exception e) {}

		EventQueue.invokeLater(new Runnable(){
        public void run() {
            	GUILaboratorio gui = new GUILaboratorio();
            }
        });
	}
}
