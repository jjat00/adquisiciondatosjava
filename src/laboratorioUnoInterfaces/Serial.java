package laboratorioUnoInterfaces;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.TooManyListenersException;

import gnu.io.CommPortIdentifier;
import gnu.io.NoSuchPortException;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;

public class Serial {

    private static CommPortIdentifier portId;
    private static SerialPort serialPort;
    private InputStream inputStream;
    private static OutputStream outputStream;
    private Thread thread;
    private EscuchaPuertoSerial escuchaSerial;
    private byte header;
    private int estadoFsm;
    private int contadorFSM;
    private byte[] bufferFSM;

    public void abrirPuertoSerial() {
        // crear escucha a serialPort
        escuchaSerial = new EscuchaPuertoSerial();
        header = 0x55;
        estadoFsm = 0;
        contadorFSM = 0;
        bufferFSM = new byte[6];

        try {
            portId = CommPortIdentifier.getPortIdentifier("/dev/ttyUSB0"); // ttyUSB0
        } catch (NoSuchPortException e) {
        }

        try {
            serialPort = (SerialPort) portId.open("SimpleReadApp", 1000);
        } catch (PortInUseException e) {
        }
    }

    public void configurarParametrosSerial() {
        try {
            serialPort.setSerialPortParams(9600, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
        } catch (Exception e) {
        }

    }

    public void leerDatos() {
        try {
            inputStream = serialPort.getInputStream();
        } catch (IOException e) {
        }
        try {
            serialPort.addEventListener(escuchaSerial);
        } catch (TooManyListenersException e) {
        }
        serialPort.notifyOnDataAvailable(true);
    }

    public void escribirDato(String cadena) {
        try {
            outputStream = serialPort.getOutputStream();
        } catch (IOException e) {
        }
        try {
            serialPort.addEventListener(escuchaSerial);
        } catch (TooManyListenersException e) {
        }
        serialPort.notifyOnDataAvailable(true);
        try {
            outputStream.write(cadena.getBytes());
            System.out.print("numero enviado: ");
            System.out.println(cadena);
        } catch (IOException e) {
        }
    }

    public void fsm(byte trama) {
        switch (estadoFsm) {
            case 0:
                if (trama == header) {
                    estadoFsm = 1;
                } else {
                    estadoFsm = 0;
                }
                break;
            case 1:
                if (trama == header) {
                    estadoFsm = 2;
                } else {
                    estadoFsm = 0;
                }
                break;
            case 2:
                if (trama == header) {
                    estadoFsm = 3;
                } else {
                    estadoFsm = 0;
                }
                break;
            case 3:
                if (contadorFSM < 5) {
                    bufferFSM[contadorFSM] = trama;
                    contadorFSM++;
                }
                if (contadorFSM == 5) {
                    String str = new String(bufferFSM);
                    String senalAuxiliar = str.substring(0, 1);
                    Grafica.senalDigital = Integer.parseInt(senalAuxiliar);
                    System.out.println(Integer.parseInt(senalAuxiliar));
                   String senalAuxiliar1 = str.substring(1, 5);
                    System.out.println(senalAuxiliar1);
                    Grafica.senalAnaloga = Integer.parseInt(senalAuxiliar1);
                    System.out.println(Integer.parseInt(senalAuxiliar1));
                    estadoFsm = 0;
                    contadorFSM = 0;
                }
                break;
        }
    }

    public void delay(int tiempo) {
        thread = new Thread() {
            public void run() {
                try {
                    while (true) {
                        sleep(tiempo);
                    }
                } catch (InterruptedException e) {
                }
            }
        };
        thread.start();
    }

    private class EscuchaPuertoSerial implements SerialPortEventListener {
        public void serialEvent(SerialPortEvent event) {
            switch (event.getEventType()) {
                case SerialPortEvent.BI:
                case SerialPortEvent.OE:
                case SerialPortEvent.FE:
                case SerialPortEvent.PE:
                case SerialPortEvent.CD:
                case SerialPortEvent.CTS:
                case SerialPortEvent.DSR:
                case SerialPortEvent.RI:
                case SerialPortEvent.OUTPUT_BUFFER_EMPTY:
                    break;
                case SerialPortEvent.DATA_AVAILABLE:
                    byte[] readBuffer = new byte[20];
                    try {
                        int numeroDatos = 0;
                        while (inputStream.available() > 0) {
                            numeroDatos = inputStream.read(readBuffer);
                        }
                        int count = 0;
                        while (count < numeroDatos) {
                            fsm(readBuffer[count]);
                            count++;
                        }
                    } catch (Exception e) {
                    }
                    break;
            }
        }
    }
}
